/* ************************************************************************** */
/*                                                                            */
/*                   ::::::::::  ::::::::  :::::::::  ::::::::::              */
/*                  :+:        :+:    :+: :+:    :+: :+:    :+:               */
/*                 +:+              +:+  +:+    +:+ +:+                       */
/*                +#++:++#       +#+    +#++:++#:  +#++:++#+                  */
/*               +#+          +#+      +#+    +#+        +#+                  */
/*              #+#         #+#       #+#    #+# #+#    #+#                   */
/*             ########## ########## ###    ###  ########                     */
/*                                                                            */
/*                print.c                                                     */
/*                                                                            */
/*                By: glouyot <glouyot@protonmail.com>                        */
/*                                                                            */
/* ************************************************************************** */

}

int	print_cyan(int fd_out, char *str, unsigned long size)
{
	write(fd_out, TF_LIGHT_CYAN, 5);
	write(fd_out, str, size);
	write(global_fd_log(), str, size);
	return (write(fd_out, TF_RESET, 5));
}

int	print_yellow(int fd_out, char *str, unsigned long size)
{
	write(fd_out, TF_YELLOW, 5);
	write(fd_out, str, size);
	write(global_fd_log(), str, size);
	return (write(fd_out, TF_RESET, 5));
}

int	print(int fd_out, char *str, unsigned long size)
{
	write(fd_out, str, size);
	return (write(global_fd_log(), str, size));
}
