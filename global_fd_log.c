/* ************************************************************************** */
/*                                                                            */
/*                   ::::::::::  ::::::::  :::::::::  ::::::::::              */
/*                  :+:        :+:    :+: :+:    :+: :+:    :+:               */
/*                 +:+              +:+  +:+    +:+ +:+                       */
/*                +#++:++#       +#+    +#++:++#:  +#++:++#+                  */
/*               +#+          +#+      +#+    +#+        +#+                  */
/*              #+#         #+#       #+#    #+# #+#    #+#                   */
/*             ########## ########## ###    ###  ########                     */
/*                                                                            */
/*                global_fd_log.c                                             */
/*                                                                            */
/*                By: glouyot <glouyot@protonmail.com>                        */
/*                                                                            */
/* ************************************************************************** */

#include "libunit.h"
#include <fcntl.h>

int	global_fd_log(void)
{
	static int	fd = -2;

	if (fd == -2)
		if ((fd = open(LOG_PATH, O_WRONLY | O_CREAT | O_TRUNC, 0755)) == -1 &&
	write(2, TF_YELLOW "log could not be oppened or created\n" TF_RESET, 46))
			exit(-1);
	return (fd);
}
