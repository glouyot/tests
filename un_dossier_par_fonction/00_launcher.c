/* ************************************************************************** */
/*                                                                            */
/*                   ::::::::::  ::::::::  :::::::::  ::::::::::              */
/*                  :+:        :+:    :+: :+:    :+: :+:    :+:               */
/*                 +:+              +:+  +:+    +:+ +:+                       */
/*                +#++:++#       +#+    +#++:++#:  +#++:++#+                  */
/*               +#+          +#+      +#+    +#+        +#+                  */
/*              #+#         #+#       #+#    #+# #+#    #+#                   */
/*             ########## ########## ###    ###  ########                     */
/*                                                                            */
/*                00_launcher.c                                               */
/*                                                                            */
/*                By: glouyot <glouyot@protonmail.com>                        */
/*                                                                            */
/* ************************************************************************** */

#include "libunit.h"
#include "../tests_project.h"

int	fonction_launcher(void)
{
	t_unit_test	*testlist;

	print(1, "STRCMP:\n", 8);
	testlist = NULL;
	load_test(&testlist, "nom du premier test", &nom_du_premier_test,
			create_signal_code(0, 0));
	return (launch_test(&testlist));
}